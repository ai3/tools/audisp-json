module git.autistici.org/ai3/tools/audisp-json

go 1.15

require (
	github.com/elastic/go-libaudit/v2 v2.5.0
	github.com/stretchr/testify v1.8.1 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
